const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');
const debug = require('debug')('productcatalog:app');

const routes = require('./src/routes');
const models = require('./src/database/models');
const { errorHandler } = require('./src/middlewares/errorHandler');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(helmet());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use(errorHandler);

// Connect to Database
models.sequelize.authenticate()
  .then(() => {
    debug('DB connection has been established successfully.');
  })
  .catch((err) => {
    debug('Unable to connect to the database:', err);
    return process.exit();
  });

module.exports = app;
