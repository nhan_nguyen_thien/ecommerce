const { expect } = require('chai');
const faker = require('faker');
const Sequelize = require('sequelize');

const productRepository = require('../../../src/repositories/product.repository');

const { Op } = Sequelize;

describe('Repository:product #unit#repository#product', () => {
  describe('#generateWhereQuery', () => {
    it('1. When receive keyword -> return query object with one condition is name iLike keyword', () => {
      // Arrange
      const keyword = faker.lorem.word();
      // Act
      const whereQuery = productRepository.generateWhereQuery({ keyword });
      // Assert
      const expectNumberOfCondition = 1;
      const expectNameCondition = { [Op.iLike]: `%${keyword}%` };

      expect(Object.keys(whereQuery).length).equal(expectNumberOfCondition);
      expect(whereQuery.name).to.have.property(Op.iLike);
      expect(whereQuery.name[Op.iLike]).equal(expectNameCondition[Op.iLike]);
    });
    it('2. When receive brand(id) -> return query object with brand is brand(id)', () => {
      // Arrange
      const brand = 1;
      // Act
      const whereQuery = productRepository.generateWhereQuery({ brand });
      // Assert
      const expectNameCondition = { brandId: 1 };
      expect(whereQuery).deep.equal(expectNameCondition);
    });
    it('3. When receive params -> mapping all value to where condition and return', () => {
      // Arrange
      const brand = 1;
      const keyword = faker.lorem.word(0);
      const priceFrom = Number(faker.commerce.price(1000));
      const priceTo = Number(faker.commerce.price(priceFrom));
      // Act
      const whereQuery = productRepository.generateWhereQuery({
        brand, keyword, priceFrom, priceTo,
      });
      // Assert
      const expectNameCondition = { [Op.iLike]: `%${keyword}%` };
      const expectBrandCondition = brand;
      /**
       * const expectPriceCondition = {
       *   [Op.and]: [
       *     { [Op.gte]: priceFrom },
       *     { [Op.lte]: priceFrom },
       *   ],
       * };
      */
      expect(whereQuery.brandId).equal(expectBrandCondition);

      expect(whereQuery.name).to.have.property(Op.iLike);
      expect(whereQuery.name[Op.iLike]).equal(expectNameCondition[Op.iLike]);

      expect(whereQuery.price).to.have.property(Op.and);
      expect(whereQuery.price[Op.and].length).equal(2);
      expect(whereQuery.price[Op.and][0]).to.have.property(Op.gte);
      expect(whereQuery.price[Op.and][0][Op.gte]).equal(priceFrom);
      expect(whereQuery.price[Op.and][1]).to.have.property(Op.lte);
      expect(whereQuery.price[Op.and][1][Op.lte]).equal(priceTo);
    });
    it('4. When receive priceFrom -> return query object with price condition', () => {
      // Arrange
      const priceFrom = Number(faker.commerce.price(1000));
      // Act
      const whereQuery = productRepository.generateWhereQuery({
        priceFrom,
      });
      // Assert
      const expectPriceCondition = { [Op.gte]: priceFrom };

      expect(whereQuery.price).to.have.property(Op.gte);
      expect(whereQuery.price[Op.gte]).equal(expectPriceCondition[Op.gte]);
    });
    it('4. When receive priceTo -> return query object with price condition', () => {
      // Arrange
      const priceFrom = Number(faker.commerce.price(1000));
      // Act
      const whereQuery = productRepository.generateWhereQuery({
        priceFrom,
      });
      // Assert
      const expectPriceCondition = { [Op.gte]: priceFrom };

      expect(whereQuery.price).to.have.property(Op.gte);
      expect(whereQuery.price[Op.gte]).equal(expectPriceCondition[Op.gte]);
    });
  });
});
