const { expect } = require('chai');
const faker = require('faker');
const sandbox = require('sinon').createSandbox();
const request = require('../../utils/request');

// controller
const productCatalogController = require('../../../src/controllers/productCatalog.controller');
// Service dependencies
const productCatalogService = require('../../../src/services/productCatalog.service');
const customerActivityService = require('../../../src/services/customerActivity.service');

describe('Controller:productCatalog #unit#controller#productCatalog', () => {
  afterEach(() => {
    sandbox.restore();
  });
  describe('#getProductCatalog', () => {
    it('1. When parameters is valid -> res has status 200 & json data is right', async () => {
      // Arrange
      const brandsExpect = [{ id: 1, name: faker.company.companyName() }];
      const productsExpect = [{ id: 1, name: faker.commerce.productName() }];
      sandbox.stub(productCatalogService, 'getBrandInCatalog').callsFake(() => brandsExpect);
      sandbox.stub(productCatalogService, 'getProductInCatalog').callsFake(() => productsExpect);
      const mockRes = request.mockResponse(sandbox);
      // Act
      await productCatalogController.getProductCataLog({}, mockRes, {});
      // Assert
      expect(mockRes.status.calledOnce).equal(true);
      expect(mockRes.status.calledWith(200)).equal(true);
      expect(mockRes.json.calledWith({
        brands: brandsExpect, products: productsExpect,
      })).equal(true);
    });
    it('2. When service is throw error -> res should not called and next will be call with error', async () => {
      // Arrange
      const expectError = new Error(faker.lorem.sentences());
      sandbox.stub(productCatalogService, 'getBrandInCatalog').throwsException(expectError);
      sandbox.stub(productCatalogService, 'getProductInCatalog').callsFake(() => {});
      const mockRes = request.mockResponse(sandbox);
      const mockNext = request.mockNext(sandbox);
      // Act
      await productCatalogController.getProductCataLog({}, mockRes, mockNext);
      // Assert
      expect(mockRes.status.notCalled).equal(true);
      expect(mockNext.calledOnce).equal(true);
      expect(mockNext.calledWith(expectError)).equal(true);
    });
  });
  describe('#searchProductInCatalog', () => {
    it('1. When query is valid -> res return status 200 & json data is right', async () => {
      // Arrange
      const expectProducts = {
        items: [{ id: 1, name: faker.commerce.productName() }], size: 10, page: 1, totalPage: 10,
      };
      sandbox.stub(productCatalogService, 'searchProduct').callsFake(() => expectProducts);
      sandbox.stub(customerActivityService, 'createCustomerActivity').callsFake(() => {});
      const mockRes = request.mockResponse(sandbox);
      // Act
      await productCatalogController.searchProductInCatalog({}, mockRes, {});
      // Assert
      expect(mockRes.status.calledOnce).equal(true);
      expect(mockRes.status.calledWith(200)).equal(true);
      expect(mockRes.json.calledWith(expectProducts)).equal(true);
    });
    it('2. When searchProduct is throw error -> next will be called with error', async () => {
      // Arrange
      const expectError = new Error(faker.lorem.sentence());
      sandbox.stub(productCatalogService, 'searchProduct').throwsException(expectError);
      const mockRes = request.mockResponse(sandbox);
      const mockNext = request.mockNext(sandbox);
      // Act
      await productCatalogController.searchProductInCatalog({}, mockRes, mockNext);
      // Assert
      expect(mockRes.status.notCalled).equal(true);
      expect(mockNext.calledOnceWith(expectError)).equal(true);
    });
    it('3. when receive query -> must pass query value to productCatalogService.searchProduct', async () => {
      // Arrange
      const expectQuery = {
        keyword: faker.lorem.word(),
        brand: faker.random.number(),
        priceFrom: faker.random.number(),
        priceTo: faker.random.number(),
        size: faker.random.number(),
        page: faker.random.number(),
        colour: faker.random.number(),
      };
      const mockRes = request.mockResponse(sandbox);
      const mockNext = request.mockNext(sandbox);
      const searchProductStub = sandbox.stub(productCatalogService, 'searchProduct');
      sandbox.stub(customerActivityService, 'createCustomerActivity').callsFake(() => {});
      // Act
      await productCatalogController
        .searchProductInCatalog({ query: expectQuery }, mockRes, mockNext);
      // Assert
      expect(searchProductStub.calledOnceWith(expectQuery)).equal(true);
    });
  });
});
