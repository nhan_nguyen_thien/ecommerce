const { expect } = require('chai');
const faker = require('faker');
const sandbox = require('sinon').createSandbox();
const request = require('../../utils/request');

// controller
const productController = require('../../../src/controllers/product.controller');
// Service dependencies
const productService = require('../../../src/services/product.service');
const customerActivityService = require('../../../src/services/customerActivity.service');

describe('Controller:product #unit#controller#product', () => {
  afterEach(() => {
    sandbox.restore();
  });
  describe('#getProductById', () => {
    it('1. When parameters is valid -> res has status 200 & json data is same as product send from productService', async () => {
      // Arrange
      const productExpect = { id: 1, name: faker.company.companyName() };
      sandbox.stub(productService, 'getProductById').callsFake(() => productExpect);
      sandbox.stub(customerActivityService, 'createCustomerActivity').callsFake(() => {});
      const mockRes = request.mockResponse(sandbox);
      // Act
      await productController.getProductById({ params: { id: 1 } }, mockRes, {});
      // Assert
      expect(mockRes.status.calledOnceWith(200)).equal(true);
      expect(mockRes.json.calledWith(productExpect)).equal(true);
    });
    it('1. When parameters is valid -> controller must called customerService to create new record activity', async () => {
      // Arrange
      const productExpect = { id: 1, name: faker.company.companyName() };
      sandbox.stub(productService, 'getProductById').callsFake(() => productExpect);
      const createCustomerActivityStub = sandbox.stub(customerActivityService, 'createCustomerActivity').callsFake(() => {});
      const mockRes = request.mockResponse(sandbox);
      // Act
      await productController.getProductById({ params: { id: 1 } }, mockRes, {});
      // Assert
      const expectActivity = { activity: 'view-detail', activityInDetail: { id: productExpect.id } };
      expect(createCustomerActivityStub.calledOnceWith(
        expectActivity.activity,
        expectActivity.activityInDetail,
      )).equal(true);
    });
  });
});
