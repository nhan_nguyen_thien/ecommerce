const { expect } = require('chai');
const faker = require('faker');
const sandbox = require('sinon').createSandbox();
const constants = require('../../../src/constants');

// controller
const productCatalogService = require('../../../src/services/productCatalog.service');
// Service dependencies
const productRepository = require('../../../src/repositories/product.repository');
const brandRepository = require('../../../src/repositories/brand.repository');

describe('Service:productCatalog #unit#service#productCatalog', () => {
  afterEach(() => {
    sandbox.restore();
  });
  describe('#searchProduct', () => {
    it('1. When receive query -> Send query object to productRepository.searchProduct', async () => {
      // Arrange
      const query = {
        keyword: faker.lorem.word(),
        brand: faker.random.number(),
        priceFrom: faker.random.number(),
        priceTo: faker.random.number(),
        size: faker.random.number(),
        page: faker.random.number(),
        colour: faker.random.number(),
      };

      const searchProductStub = sandbox.stub(productRepository, 'searchProduct').callsFake(() => ({ rows: [], count: 0 }));
      // Act
      await productCatalogService.searchProduct(query);
      // Assert
      expect(searchProductStub.calledOnceWith(query)).equal(true);
    });
    it('2. When receive query missing page or size parameters -> should using default value', async () => {
      // Arrange
      const query = {
        keyword: faker.lorem.word(),
        brand: faker.random.number(),
        priceFrom: faker.random.number(),
        priceTo: faker.random.number(),
        colour: faker.random.number(),
      };
      const searchProductStub = sandbox
        .stub(productRepository, 'searchProduct')
        .callsFake(() => ({ rows: [], count: 0 }));
      // Act
      await productCatalogService.searchProduct(query);
      // Assert
      expect(searchProductStub.calledOnceWith({
        ...query,
        page: constants.PRODUCT.DEFAULT_PAGE_NUMBER,
        size: constants.PRODUCT.DEFAULT_PAGE_SIZE,
      })).equal(true);
    });
    it('3. When receive query -> return formated data', async () => {
      // Arrange
      const query = {
        keyword: faker.lorem.word(),
        brand: faker.random.number(),
        priceFrom: faker.random.number(),
        priceTo: faker.random.number(),
        colour: faker.random.number(),
        page: 0,
        size: 2,
      };
      const expectCount = 3;
      const expectTotalPage = 2;
      sandbox.stub(productRepository, 'searchProduct').callsFake(() => ({
        rows: [1, 2, 3],
        count: expectCount,
      }));
      // Act
      const result = await productCatalogService.searchProduct(query);
      // Assert
      expect(result).to.have.property('items');
      expect(result.items.length).equal(3);
      expect(result.page).equal(query.page);
      expect(result.size).equal(query.size);
      expect(result.totalPage).equal(expectTotalPage);
    });
  });
  describe('#getProductInCatalog', () => {
    it('1. When called -> return array of products', async () => {
      // Arrange
      const expectProducts = [1, 2, 3];
      sandbox.stub(productRepository, 'searchProduct').callsFake(() => ({
        rows: expectProducts,
        count: 3,
      }));
      // Act
      const result = await productCatalogService.getProductInCatalog();
      // Assert
      expect(result).equal(expectProducts);
    });
  });
  describe('#getBrandInCatalog', () => {
    it('1. When called -> return all brands in array format', async () => {
      // Arrange
      const expectBrands = [
        { id: 1, name: faker.company.companyName() },
        { id: 2, name: faker.company.companyName() },
        { id: 3, name: faker.company.companyName() },
        { id: 4, name: faker.company.companyName() },
      ];
      sandbox.stub(brandRepository, 'getBrands').callsFake(() => ({
        rows: expectBrands,
        count: 4,
      }));
      // Act
      const result = await productCatalogService.getBrandInCatalog();
      // Assert
      expect(result).equal(expectBrands);
    });
  });
});
