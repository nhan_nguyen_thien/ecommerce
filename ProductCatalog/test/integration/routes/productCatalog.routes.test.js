const supertest = require('supertest');
const sandbox = require('sinon').createSandbox();
const { expect } = require('chai');
const app = require('../../../app');

const customerActivityService = require('../../../src/services/customerActivity.service');

describe('Routes:productCatalog #integration#router#productCatalog', () => {
  afterEach(() => {
    sandbox.restore();
  });
  describe('#GET /product-catalog', () => {
    it('1. When request, then it must response the catalog data with status 200', async () => {
      // Act
      const response = await supertest(app).get('/product-catalog/');
      // Assert
      const expectNumberOfProduct = 20;
      expect(response.status).equal(200);
      expect(response.body).to.have.property('products');
      expect(response.body).to.have.property('brands');
      expect(response.body.products).is.an('array');
      expect(response.body.brands).is.an('array');
      expect(response.body.products.length).equal(expectNumberOfProduct);

      // check product format
      const exactProduct = response.body.products[0];
      expect(exactProduct).to.have.property('id');
      expect(exactProduct).to.have.property('name');
      expect(exactProduct).to.have.property('price');
      expect(exactProduct).to.have.property('brand');
      expect(exactProduct.brand).to.have.property('id');
      expect(exactProduct.brand).to.have.property('name');
      expect(exactProduct).to.have.property('productColours');
      expect(exactProduct.productColours).is.an('array');

      // check brand format
      const exactBrand = response.body.brands[0];
      expect(exactBrand).to.have.property('id');
      expect(exactBrand).to.have.property('name');
    });
  });
  describe('#GET /product-catalog/search', () => {
    it('1. When request with query is valid, then it must response the product list with status 200', async () => {
      // Arrange
      sandbox.stub(customerActivityService, 'createCustomerActivity').callsFake(() => {});
      // Act
      const response = await supertest(app).get('/product-catalog/search/');
      // Assert
      expect(response.status).equal(200);
      expect(response.body).to.have.property('items');
      expect(response.body).to.have.property('page');
      expect(response.body).to.have.property('size');
      expect(response.body).to.have.property('totalPage');
    });
    it('2. When request with query is invalid, then it must response the error with status 400', async () => {
      const response = await supertest(app).get('/product-catalog/search/')
        .query({ priceFrom: 10000, priceTo: 5000 });
      expect(response.status).equal(400);
      expect(response.body).to.have.property('error');
      expect(response.body).to.have.property('errorType');
      expect(response.body).to.have.property('message');
    });
  });
});
