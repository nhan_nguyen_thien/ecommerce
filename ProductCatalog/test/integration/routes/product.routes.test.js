const supertest = require('supertest');
const sandbox = require('sinon').createSandbox();
const { expect } = require('chai');
const app = require('../../../app');

const customerActivityService = require('../../../src/services/customerActivity.service');

describe('Routes:product #integration#router#product', () => {
  afterEach(() => {
    sandbox.restore();
  });
  describe('#GET /product/:id', () => {
    it('1. When request with productId, then it must response the product with status 200', async () => {
      // Act
      const response = await supertest(app).get('/product/2');
      // Assert
      const expectProductId = 2;
      const exactProduct = response.body;
      expect(response.status).equal(200);
      expect(exactProduct).to.have.property('id');
      expect(exactProduct.id).equal(expectProductId);

      // check product format
      expect(exactProduct).to.have.property('id');
      expect(exactProduct).to.have.property('name');
      expect(exactProduct).to.have.property('price');
      expect(exactProduct).to.have.property('brand');
      expect(exactProduct.brand).to.have.property('id');
      expect(exactProduct.brand).to.have.property('name');
      expect(exactProduct).to.have.property('productColours');
      expect(exactProduct.productColours).is.an('array');
    });
    it('2. When request with productId and not product match, then it must response body null with status 200', async () => {
      // Act
      const response = await supertest(app).get('/product/1000');
      // Assert
      const exactProduct = response.body;
      const expectProduct = null;
      expect(response.status).equal(200);
      expect(exactProduct).equal(expectProduct);
    });
    it('2. When request with productId is invalid (not integer), then it must body Error with status 400', async () => {
      // Act
      const response = await supertest(app).get('/product/abc');
      // Assert
      expect(response.status).equal(400);
      expect(response.body).to.have.property('errorType');
    });
  });
});
