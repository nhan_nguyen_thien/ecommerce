### Setup project on your localhost

+ Clone
+ `npm i`
+ create `.env` file from `.env.example`

### Using scripts

+ Seeder database:
  + seeder all `npm run db:seed:all`
+ Undo Seeder:
  + undo all: `npm run db:seed:undo:all`
  + undo specific: `npm run db:seed:undo -- --seed seeder-name`
