module.exports = {
  up: async (queryInterface) => queryInterface.bulkInsert('productColours', [
    { id: 1, productId: 1, colourId: 1 },
    { id: 2, productId: 1, colourId: 3 },
    { id: 3, productId: 2, colourId: 3 },
    { id: 4, productId: 3, colourId: 4 },
    { id: 5, productId: 4, colourId: 1 },
  ]),
  down: async (queryInterface) => queryInterface.bulkDelete('productColours', {}),
};
