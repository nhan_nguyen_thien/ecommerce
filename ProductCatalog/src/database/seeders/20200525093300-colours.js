module.exports = {
  up: (queryInterface) => queryInterface.bulkInsert('colours', [
    { id: 1, name: 'Grey' },
    { id: 2, name: 'Black' },
    { id: 3, name: 'White' },
    { id: 4, name: 'Gold' },
  ]),
  down: async (queryInterface) => queryInterface.bulkDelete('colours', {}),
};
