const faker = require('faker');

module.exports = {
  up: async (queryInterface) => {
    const products = [
      {
        id: 1, name: 'iPhone', price: 20000000, brandId: 1,
      },
      {
        id: 2, name: 'iPad', price: 11000000, brandId: 1,
      },
      {
        id: 3, name: 'MSI1', price: 21000000, brandId: 3,
      },
      {
        id: 4, name: 'MSI2', price: 22000000, brandId: 3,
      },
    ]
    // faker some products
    for (var i = 5; i < 100; i++) {
      products.push({
        id: i, name: faker.commerce.productName(), price: Number(faker.commerce.price(1000, 1000000000)), brandId: Math.floor(Math.random() * 8) + 1,
      })
    }
    return queryInterface.bulkInsert('products', products)
  },
  down: async (queryInterface) => queryInterface.bulkDelete('products', {}),
};
