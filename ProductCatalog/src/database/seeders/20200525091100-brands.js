module.exports = {
  up: (queryInterface) => queryInterface.bulkInsert('brands', [
    { id: 1, name: 'Apple' },
    { id: 2, name: 'DELL' },
    { id: 3, name: 'MSI' },
    { id: 4, name: 'NOKIA' },
    { id: 5, name: 'Sony' },
    { id: 6, name: 'SamSung' },
    { id: 7, name: 'Oppo' },
    { id: 8, name: 'Asus' },
  ]),
  down: async (queryInterface) => queryInterface.bulkDelete('brands', {}),
};
