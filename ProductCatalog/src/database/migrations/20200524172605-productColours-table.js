module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface.createTable('productColours', {
    id: {
      primaryKey: true,
      type: Sequelize.INTEGER,
      autoIncrement: true,
    },
    colourId: {
      type: Sequelize.SMALLINT,
      allowNull: false,
      references: {
        model: 'colours',
        key: 'id',
      },
    },
    productId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'products',
        key: 'id',
      },
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('productColours'),
};
