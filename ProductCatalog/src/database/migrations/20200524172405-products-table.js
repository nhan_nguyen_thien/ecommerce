module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface.createTable('products', {
    id: {
      primaryKey: true,
      type: Sequelize.INTEGER,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING(255),
      allowNull: false,
    },
    price: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    brandId: {
      type: Sequelize.SMALLINT,
      allowNull: true,
      references: {
        model: 'brands',
        key: 'id',
      },
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('products'),
};
