module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface.createTable('brands', {
    id: {
      primaryKey: true,
      type: Sequelize.SMALLINT,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING(128),
      allowNull: false,
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('brands'),
};
