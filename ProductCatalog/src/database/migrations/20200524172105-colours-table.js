module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface.createTable('colours', {
    id: {
      primaryKey: true,
      type: Sequelize.SMALLINT,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING(32),
      allowNull: false,
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('colours'),
};
