module.exports = (sequelize, DataTypes) => {
  const Colours = sequelize.define('colours', {
    id: {
      primaryKey: true,
      type: DataTypes.SMALLINT,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(32),
      allowNull: false,
    },
  }, {
    timestamps: false,
  });
  Colours.associate = (models) => {
    Colours.hasMany(models.productColours, { foreignKey: 'colourId', sourceKey: 'id' });
  };
  return Colours;
};
