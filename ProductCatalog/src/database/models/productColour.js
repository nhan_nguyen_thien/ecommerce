module.exports = (sequelize, DataTypes) => {
  const ProductColours = sequelize.define('productColours', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true,
    },
    colourId: {
      type: DataTypes.SMALLINT,
      allowNull: false,
    },
    productId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });
  ProductColours.associate = (models) => {
    ProductColours.belongsTo(models.colours, { foreignKey: 'colourId', targetKey: 'id' });
    ProductColours.belongsTo(models.products, { foreignKey: 'productId', targetKey: 'id' });
  };
  return ProductColours;
};
