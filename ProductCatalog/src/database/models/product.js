module.exports = (sequelize, DataTypes) => {
  const Products = sequelize.define('products', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    brandId: {
      type: DataTypes.SMALLINT,
      allowNull: true,
    },
  }, {
    timestamps: false,
  });
  Products.associate = (models) => {
    Products.belongsTo(models.brands, { foreignKey: 'brandId', targetKey: 'id' });
    Products.hasMany(models.productColours, { foreignKey: 'productId', sourceKey: 'id' });
  };
  return Products;
};
