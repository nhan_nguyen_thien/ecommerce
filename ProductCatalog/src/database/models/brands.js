module.exports = (sequelize, DataTypes) => {
  const Brands = sequelize.define('brands', {
    id: {
      primaryKey: true,
      type: DataTypes.SMALLINT,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
  }, {
    timestamps: false,
  });
  Brands.associate = (models) => {
    Brands.hasMany(models.products, { foreignKey: 'brandId', sourceKey: 'id' });
  };
  return Brands;
};
