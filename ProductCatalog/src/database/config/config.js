const dotenv = require('dotenv');

/** Load environment */
dotenv.config({ path: '.env' });
const configs = {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  dialect: process.env.DB_DIALECT,
  database: process.env.DB_DATABASE,
  migrationStorage: 'sequelize',
  seederStorage: 'sequelize',
  logging: false,
  freezeTableName: true,
};

module.exports = {
  development: configs,
  testing: configs,
  production: configs,
};
