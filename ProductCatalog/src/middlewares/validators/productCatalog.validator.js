const Joi = require('@hapi/joi');

const searchProductInCatalogSchema = Joi.object({
  keyword: Joi.string().min(1),
  brand: Joi.number().integer().min(0),
  priceFrom: Joi.number().integer().min(0),
  priceTo: Joi.number()
    .integer()
    .min(0)
    .greater(Joi.ref('priceFrom', { adjust: (v) => v || 0 })),
  size: Joi.number().integer().min(0),
  page: Joi.number().integer().min(0),
  colour: Joi.number().integer().min(0),
});

module.exports = {
  searchProductInCatalogSchema,
};
