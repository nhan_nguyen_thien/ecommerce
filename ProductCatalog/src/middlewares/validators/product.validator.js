const Joi = require('@hapi/joi');

const getProductByIdSchema = Joi.object({
  id: Joi.number().integer().min(1),
});

module.exports = {
  getProductByIdSchema,
};
