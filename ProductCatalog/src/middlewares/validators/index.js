const validateSchema = (schema, propName) => (req, res, next) => {
  const { error } = schema.validate(req[propName]);

  if (error) return next(error);
  return next();
};

module.exports = {
  validateSchema,
};
