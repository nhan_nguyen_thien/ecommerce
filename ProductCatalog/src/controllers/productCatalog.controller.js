const productCatalogService = require('../services/productCatalog.service');
const customerActivityService = require('../services/customerActivity.service');

/**
 * The Catalog need show some products and some brands
 */
const getProductCataLog = async (req, res, next) => {
  try {
    const [products, brands] = await Promise.all([
      productCatalogService.getProductInCatalog(),
      productCatalogService.getBrandInCatalog(),
    ]);
    res.status(200).json({ products, brands });
  } catch (error) {
    next(error);
  }
};

const searchProductInCatalog = async (req, res, next) => {
  try {
    const result = await productCatalogService.searchProduct(req.query);
    res.status(200).json(result);
    customerActivityService.createCustomerActivity('search', req.query);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getProductCataLog,
  searchProductInCatalog,
};
