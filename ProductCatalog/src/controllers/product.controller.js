const productService = require('../services/product.service');
const customerActivityService = require('../services/customerActivity.service');

const getProductById = async (req, res, next) => {
  try {
    const product = await productService.getProductById(req.params.id);
    res.status(200).json(product);
    customerActivityService.createCustomerActivity('view-detail', { id: req.params.id });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getProductById,
};
