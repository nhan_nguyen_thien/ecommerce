const PRODUCT = {
  DEFAULT_PAGE_SIZE: 10,
  DEFAULT_PAGE_NUMBER: 0,
};

const CATALOG_PRODUCT = {
  NUMBER_OF_PRODUCTS: 20,
};

const ERROR = {
  TYPES: {
    VALIDATION: 'validation-error',
  },
};

module.exports = {
  PRODUCT,
  CATALOG_PRODUCT,
  ERROR,
};
