const express = require('express');
const productCatalogController = require('../controllers/productCatalog.controller');
const schema = require('../middlewares/validators/productCatalog.validator');
const validatorMiddleware = require('../middlewares/validators');

const router = express.Router();

router.get('/', productCatalogController.getProductCataLog);

router.get('/search',
  validatorMiddleware.validateSchema(schema.searchProductInCatalogSchema, 'query'),
  productCatalogController.searchProductInCatalog);

module.exports = router;
