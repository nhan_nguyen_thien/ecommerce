const express = require('express');

const productCatalogRoutes = require('./productCatalog.router');
const productRoutes = require('./product.router');

const router = express.Router();

router.use('/product-catalog', productCatalogRoutes);
router.use('/product', productRoutes);

module.exports = router;
