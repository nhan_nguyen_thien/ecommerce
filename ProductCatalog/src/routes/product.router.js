const express = require('express');
const productController = require('../controllers/product.controller');
const schema = require('../middlewares/validators/product.validator');
const validatorMiddleware = require('../middlewares/validators');

const router = express.Router();

router.get('/:id',
  validatorMiddleware.validateSchema(schema.getProductByIdSchema, 'params'),
  productController.getProductById);

module.exports = router;
