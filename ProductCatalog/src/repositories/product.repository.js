const Sequelize = require('sequelize');
const models = require('../database/models');

const { Op } = Sequelize;

/** generateSearchQuery : (Object) => (Object) */
const generateWhereQuery = (searchObj) => {
  const {
    keyword,
    brand,
    priceFrom,
    priceTo,
  } = searchObj;
  const query = {};
  if (keyword) {
    query.name = { [Op.iLike]: `%${keyword}%` };
  }
  if (brand) {
    query.brandId = brand;
  }
  if (priceFrom && priceTo) {
    query.price = {
      [Op.and]: [
        { [Op.gte]: priceFrom },
        { [Op.lte]: priceTo },
      ],
    };
  } else if (priceFrom) {
    query.price = { [Op.gte]: priceFrom };
  } else if (priceTo) {
    query.price = { [Op.lte]: priceTo };
  }
  return query;
};

/**
 * searchProduct : Object => Promise<{count: Number, rows: [Product]}>
 */
const searchProduct = (searchObj) => {
  const {
    size,
    page,
    colour,
  } = searchObj;
  //
  const whereObject = generateWhereQuery(searchObj);
  return models.products.findAndCountAll({
    where: whereObject,
    limit: size,
    offset: page * size,
    include: [
      {
        model: models.brands,
      },
      {
        model: models.productColours,
        ...(colour ? { where: { colourId: colour } } : {}),
        attributes: ['colourId'],
        include: [{
          model: models.colours,
          attributes: ['name'],
        }],
      },
    ],
  });
};

// getProductById : String => Promise<Product | null>
const getProductById = (id) => models.products.findOne({
  where: { id },
  include: [
    { model: models.brands },
    {
      model: models.productColours,
      attributes: ['colourId'],
      include: [{
        model: models.colours,
        attributes: ['name'],
      }],
    },
  ],
});

module.exports = {
  searchProduct,
  generateWhereQuery,
  getProductById,
};
