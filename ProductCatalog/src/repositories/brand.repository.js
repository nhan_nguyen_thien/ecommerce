const models = require('../database/models');

/**
 * getBrands : Object => [Brand]
 */
const getBrands = () => models.brands.findAndCountAll({
  where: {},
});

module.exports = {
  getBrands,
};
