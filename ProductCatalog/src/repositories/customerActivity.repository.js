const axios = require('axios');

const customerActivityEndpoint = process.env.ENDPOINT_CUSTOMER_ACTIVITY;
const customerActivityInstance = axios.create({ baseURL: customerActivityEndpoint });

// createOne : ({ activity: String, activityInDetail: Object }) => CustomerActivity
const createOne = async (data) => {
  const result = await customerActivityInstance.post('/customer-activity', data);
  return result;
};

module.exports = {
  createOne,
};
