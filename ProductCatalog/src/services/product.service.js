const productRepository = require('../repositories/product.repository');

// getProductById : (String) -> Promise<Product | null>
const getProductById = async (id) => productRepository.getProductById(id);

module.exports = {
  getProductById,
};
