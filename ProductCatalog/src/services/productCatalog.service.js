const R = require('ramda');

const constants = require('../constants');
const productRepository = require('../repositories/product.repository');
const brandRepository = require('../repositories/brand.repository');

/**
 * searchProduct : (Object) => {items: [Product], size: Number, page: Number, totalPage: Number}
 */
const searchProduct = async (query) => {
  const {
    size = constants.PRODUCT.DEFAULT_PAGE_SIZE,
    page = constants.PRODUCT.DEFAULT_PAGE_NUMBER,
  } = query;
  const products = await productRepository.searchProduct({
    ...R.pick(['keyword', 'brand', 'priceFrom', 'priceTo', 'colour'], query),
    size,
    page,
  });
  return {
    items: products.rows,
    page,
    size,
    totalPage: Math.ceil(products.count / size),
  };
};

/**
 * Let say the business team want get catalog by getting 20 product
 * getProductCatalog : () => {products: [Product]}
 */
const getProductInCatalog = async () => {
  const products = await productRepository.searchProduct({
    size: constants.CATALOG_PRODUCT.NUMBER_OF_PRODUCTS,
    page: 0,
  });
  return products.rows;
};

/**
 * getBrandInCatalog : () => [Brand]
 */
const getBrandInCatalog = async () => {
  const brands = await brandRepository.getBrands();
  return brands.rows;
};

module.exports = {
  searchProduct,
  getProductInCatalog,
  getBrandInCatalog,
};
