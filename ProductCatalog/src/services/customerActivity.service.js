const customerActivityRepository = require('../repositories/customerActivity.repository');

const createCustomerActivity = async (activity, activityInDetail) => {
  const result = await customerActivityRepository.createOne({ activity, activityInDetail });
  return result;
};

module.exports = {
  createCustomerActivity,
};
