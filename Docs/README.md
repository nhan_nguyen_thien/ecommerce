# Ecommerce Project

## Ubiquitous Language

+ Product Catalog
+ Product
+ Colour
+ Brand
+ Customer Activity

## Bounded Context

![Bounded Context](./bounded-context.png)

+ Because the Customer Activity is not a core concept and these type can be out-sourcing or from a vender. So we will move it out of core domain
and define as Generic sub-domain.

## Architect

![Architect](./architect.png)

+ The client will get resources product catalog by request to api-gateway. And then this request will forward to the product-catalog service.
+ Communication between we can use messing or REST. In this phase we will use REST to communication.
+ Database is seperate for each services.
+ For product-catalog service, I use relational DB
+ For customer-activity service, I use non-relational DB

## Database Design

![Database Design](./database-design.png)

## Code structure

+ Service_Project
  + `index.js`
  + `app.js`
  + `src`
    + `constants` (constants values)
    + `database`
      + `config`
      + `models`
      + `migration`
      + `seeder`
    + `middleware` (some useful middlewares will be reused)
      + validators (validate schema of request)
      + errorHandler
    + `routes`
    + `controllers` (include all controller. This will handle the router.
      Controller will receive request and them known which services need to be called to serve that request)
    + `services` (the services will hold the business logic to handle request, to access to data layer or infrastructure with database, they need to call repository)
    + `respositories` (will handle request from service to data layer.
      This layer will de-coupling the business logic and data layer. And treat database as attached resources)
  + `test`
    + `unit` (include unit test - the structure is same as *src* folder for easy lookup)
    + `integration` (include integration test - the structure is same as *src* folder for easy lookup)

## Run in local

+ `git clone git@bitbucket.org:nhan_nguyen_thien/ecommerce.git`
+ `cd ecommerce`
+ `cd ./DockerCompose`
+ `sudo docker-compose up --build`

## API Docs

Please copy content in file `api.yml` to this editor online [Swagger](https://editor.swagger.io/)

## Sequence Diagram

### GET /product-catalog

+ ![Diagram](./sequenceDiagram/get_catalog_product.png)

### GET /product-catalog/search

+ ![Diagram](./sequenceDiagram/get_product_catalog_search.png)

### GET /product/:id

+ ![Diagram](./sequenceDiagram/get_product_by_id.png)

### POST /customer-activity

+ ![Diagram](./sequenceDiagram/post_customer_activity.png)

## API test

### API GET /product-catalog

+ `curl http://localhost:3000/product-catalog | json_pp`

### API GET /product-catalog/search

+ `curl "http://localhost:3000/product-catalog/search?keyword=Phone" | json_pp`
+ `curl "http://localhost:3000/product-catalog/search?keyword=Phone&priceFrom=10000000" | json_pp`
+ `curl "http://localhost:3000/product-catalog/search?keyword=Phone&priceFrom=10000000&priceTo=9000000" | json_pp`
+ `curl "http://localhost:3000/product-catalog/search?keyword=Phone&priceFrom=10000000&priceTo=25000000&brand=1" | json_pp`
+ `curl "http://localhost:3000/product-catalog/search?colour=3" | json_pp`

### API GET /product/:id

+ `curl "http://localhost:3000/product/1" | json_pp`
+ `curl "http://localhost:3000/product/1000" | json_pp`
+ `curl "http://localhost:3000/product/abc" | json_pp`

### API POST /customer-activity

+ `curl -X POST -H "Content-Type: application/json" -d '{"activity": "search", "activityInDetail": {"keyword": "phone"}}' http://localhost:3001/customer-activity | json_pp`
+ `curl -X POST -H "Content-Type: application/json" -d '{"activity": "create", "activityInDetail": {"keyword": "phone"}}' http://localhost:3001/customer-activity | json_pp`
+ `curl -X POST -H "Content-Type: application/json" -d '{"activity": "search"}' http://localhost:3001/customer-activity | json_pp`
