const { expect } = require('chai');
const faker = require('faker');
const sandbox = require('sinon').createSandbox();

// controller
const customerActivityService = require('../../../src/services/customerActivity.service');
// Service dependencies
const customerActivityRepository = require('../../../src/repositories/customerActivity.repository');

describe('Service:customerActivity #unit#service#customerActivity', () => {
  afterEach(() => {
    sandbox.restore();
  });
  describe('#createCustomerActivity', () => {
    it('1. When receive activity data -> Send query object to customerActivityRepository.createOne', async () => {
      // Arrange
      const body = {
        activity: 'search',
        activityInDetail: {
          keyword: faker.lorem.word(),
          priceFrom: Number(faker.commerce.price(1000)),
        },
      };
      const createOneStub = sandbox.stub(customerActivityRepository, 'createOne').callsFake(() => (body));
      // Act
      await customerActivityService.createCustomerActivity(body);
      // Assert
      expect(createOneStub.calledOnceWith(body)).equal(true);
    });
    it('3. When receive query -> return created object', async () => {
      // Arrange
      const body = {
        activity: 'search',
        activityInDetail: {
          keyword: faker.lorem.word(),
          priceFrom: Number(faker.commerce.price(1000)),
        },
      };
      sandbox.stub(customerActivityRepository, 'createOne').callsFake(() => body);
      // Act
      const exact = await customerActivityService.createCustomerActivity(body);
      // Assert
      const expectValue = body;
      expect(exact).deep.equal(expectValue);
    });
  });
});
