const { expect } = require('chai');
const faker = require('faker');
const sandbox = require('sinon').createSandbox();
const request = require('../../utils/request');

// controller
const customerActivityController = require('../../../src/controllers/customerActivity.controller');
// Service dependencies
const customerActivityService = require('../../../src/services/customerActivity.service');

describe('Controller:customerActivity #unit#controller#customerActivity', () => {
  afterEach(() => {
    sandbox.restore();
  });
  describe('#createCustomerActivity', () => {
    it('1. When parameters is valid -> res has status 201 & body is created record', async () => {
      // Arrange
      const expectRecord = { _id: '123456', activity: 'search' };
      sandbox.stub(customerActivityService, 'createCustomerActivity').callsFake(() => expectRecord);
      const mockRes = request.mockResponse(sandbox);
      // Act
      await customerActivityController.createCustomerActivity({}, mockRes, {});
      // Assert
      expect(mockRes.status.calledOnce).equal(true);
      expect(mockRes.status.calledWith(201)).equal(true);
      expect(mockRes.json.calledWith(expectRecord)).equal(true);
    });
    it('2. When service is throw error -> res should not called and next will be call with error', async () => {
      // Arrange
      const expectError = new Error(faker.lorem.sentences());
      sandbox.stub(customerActivityService, 'createCustomerActivity').throwsException(expectError);
      const mockRes = request.mockResponse(sandbox);
      const mockNext = request.mockNext(sandbox);
      // Act
      await customerActivityController.createCustomerActivity({}, mockRes, mockNext);
      // Assert
      expect(mockRes.status.notCalled).equal(true);
      expect(mockNext.calledOnce).equal(true);
      expect(mockNext.calledWith(expectError)).equal(true);
    });
  });
});
