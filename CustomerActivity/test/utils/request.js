const mockResponse = (sandbox) => {
  const res = {};
  res.status = sandbox.stub().returns(res);
  res.json = sandbox.stub().returns(res);
  return res;
};

const mockNext = (sandbox) => sandbox.stub();

module.exports = {
  mockResponse,
  mockNext,
};
