const supertest = require('supertest');
const sandbox = require('sinon').createSandbox();
const faker = require('faker');
const { expect } = require('chai');
const app = require('../../../app');

const customerActivityRespository = require('../../../src/repositories/customerActivity.repository');

describe('Routes:customerActivity #integration#router#customerActivity', () => {
  afterEach(() => {
    sandbox.restore();
  });
  describe('#POST /customer-activity', () => {
    it('1. When request with valid data, then it must response the result with status 200', async () => {
      // Arrange
      const spyCreateOne = sandbox.spy(customerActivityRespository, 'createOne');
      const expectActivity = {
        activity: 'search',
        activityInDetail: {
          keyword: faker.lorem.word(),
          priceFrom: Number(faker.commerce.price(1000)),
        },
      };
      // Act
      const response = await supertest(app).post('/customer-activity/').send(expectActivity);
      // Assert
      expect(response.status).equal(201);
      expect(response.body).to.have.property('_id');

      // Check stored in DB Object
      const exactResult = await spyCreateOne.returnValues[0];
      expect(exactResult).to.have.property('_id');
      expect(exactResult).to.have.property('activity');
      expect(exactResult.activity).equal(expectActivity.activity);
      expect(exactResult).to.have.property('activityInDetail');
      expect(exactResult.activityInDetail).deep.equal(expectActivity.activityInDetail);
    });
    it('2. When request with query is invalid, then it must response the error with status 400', async () => {
      // Act
      const exactResult = await supertest(app).post('/customer-activity/').send({ activity: 'go' });
      //  Assert
      expect(exactResult.status).equal(400);
      expect(exactResult.body).to.have.property('error');
      expect(exactResult.body).to.have.property('errorType');
    });
  });
});
