const express = require('express');

const customerActivityRoutes = require('./customerActivity.router');

const router = express.Router();

router.use('/customer-activity', customerActivityRoutes);

module.exports = router;
