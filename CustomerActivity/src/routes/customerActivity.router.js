const express = require('express');
const customerActivityController = require('../controllers/customerActivity.controller');
const schema = require('../middlewares/validators/customerActivity.validator');
const validatorMiddleware = require('../middlewares/validators');

const router = express.Router();

router.post('/',
  validatorMiddleware.validateSchema(schema.createCustomerActivity, 'body'),
  customerActivityController.createCustomerActivity);

module.exports = router;
