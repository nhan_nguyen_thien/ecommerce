const dotenv = require('dotenv');

/** Load environment */
dotenv.config({ path: '.env' });

const DB_NAME = process.env.NODE_ENV === 'testing' ? `${process.env.DB_NAME}_testing` : process.env.DB_NAME;
const configs = {
  databaseName: DB_NAME,
  databaseUri: `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${DB_NAME}?authSource=admin&retryWrites=true&w=1`,
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
};

module.exports = {
  development: configs,
  testing: configs,
  production: configs,
};
