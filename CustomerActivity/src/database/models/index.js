const mongoose = require('mongoose');

/** Models */
const CustomerActivity = require('./customerActivity.model');

const db = {
  mongoose,
  CustomerActivity: mongoose.model('CustomerActivity', CustomerActivity),
};

module.exports = db;
