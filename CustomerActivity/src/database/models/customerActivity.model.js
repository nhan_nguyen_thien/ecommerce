const { Schema } = require('mongoose');

const CustomerActivity = new Schema({
  activity: {
    type: String, enum: ['search', 'view-detail'],
  },
  activityInDetail: {
    type: Object,
  },
  createdAt: { type: Date, default: Date.now },
});

module.exports = CustomerActivity;
