const models = require('../database/models');

/**
 * createOne : Object => CustomerActivity
 */
const createOne = (data) => models.CustomerActivity.create(data);

module.exports = {
  createOne,
};
