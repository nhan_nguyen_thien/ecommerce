const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
const debug = require('debug')('productcatalog:middleware:errorHandler');

const DatabaseError = mongoose.Error;
const ValidatorError = Joi.ValidationError;

// eslint-disable-next-line no-unused-vars
const errorHandler = (err, req, res, next) => {
  const isProduction = req.app.get('env') === 'production';
  if (!isProduction) debug(err);

  // check Error Type
  // eslint-disable-next-line prefer-const
  let result = {};
  if (err instanceof DatabaseError) {
    result.errorType = 'DatabaseError';
    result.error = isProduction ? null : err.errors;
    result.message = isProduction ? 'Database Error!' : err.message;
    result.status = 500;
  } else if (err instanceof ValidatorError) {
    result.errorType = 'ValidatorError';
    result.error = isProduction ? null : err;
    result.message = isProduction ? 'Database Error!' : err.message;
    result.status = 400;
  } else {
    // unknow
    result.errorType = 'SystemError';
    result.error = isProduction ? null : err;
    result.message = isProduction ? 'System Error!' : err.message;
    result.status = 500;
  }
  res.status(result.status).json(result);
};

module.exports = {
  errorHandler,
};
