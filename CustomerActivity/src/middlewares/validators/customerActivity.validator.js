const Joi = require('@hapi/joi');

const createCustomerActivity = Joi.object({
  activity: Joi.string().valid('search', 'view-detail'),
  activityInDetail: Joi.object().required(),
});

module.exports = {
  createCustomerActivity,
};
