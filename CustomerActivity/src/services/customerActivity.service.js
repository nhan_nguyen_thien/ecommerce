const customerActivityRepository = require('../repositories/customerActivity.repository');

/**
 * createCustomerActivity : (Object) =>
 */
const createCustomerActivity = async (body) => customerActivityRepository.createOne(body);

module.exports = {
  createCustomerActivity,
};
