const customerActivityService = require('../services/customerActivity.service');

const createCustomerActivity = async (req, res, next) => {
  try {
    const record = await customerActivityService.createCustomerActivity(req.body);
    res.status(201).json(record);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  createCustomerActivity,
};
